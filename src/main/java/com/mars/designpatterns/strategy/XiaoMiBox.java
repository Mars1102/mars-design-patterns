package com.mars.designpatterns.strategy;

/**
 * author by: mars
 * Date: 2020/3/10 23:55
 * Description: 小米魔法盒
 */
public class XiaoMiBox implements MagicBox {

    @Override
    public void makeBox() {
        System.out.println("我是小爱同学");
    }
}
