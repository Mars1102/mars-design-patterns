package com.mars.designpatterns.strategy;

/**
 * author by: mars
 * Date: 2020/3/11 0:08
 * Description:
 */
public class Test {

    public static void main(String[] args) {
        // 策略方式一：较简单，但也不可避免if..else...的情况
        //ShowMagicBox catBox = new ShowMagicBox(new CatBox());
        //ShowMagicBox xiaoMiBox = new ShowMagicBox(new XiaoMiBox());
        //catBox.showMagixBox();
        //xiaoMiBox.showMagixBox();
        // 策略方式二：
        String cat = "xiaomi";
        ShowMagicBox showMagicBox = new ShowMagicBox(MagicBoxFactory.getWantBox(cat));
        showMagicBox.showMagixBox();
    }
}
