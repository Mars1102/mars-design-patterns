package com.mars.designpatterns.strategy;

import java.util.HashMap;
import java.util.Map;

/**
 * author by: mars
 * Date: 2020/3/11 8:43
 * Description: 魔盒工厂
 */
public class MagicBoxFactory {

    private static Map<String,MagicBox> map = new HashMap<>();

    private static  final MagicBox emptyBox = new EmptyBox();

    // 初始化map，把策略put，
    // 对应到Spring中，只要注入被Spring管理的实现同一接口的实现类，
    // Spring提供了Map的方式自动注入，即可从Map中获取实现接口的不同实现类，key为实现类的名称（首字母小写）
    static {
        map.put(BoxKey.CAT,new CatBox());
        map.put(BoxKey.XIAOMI,new XiaoMiBox());
    }

    // 不希望外部new该工厂，将构造器改为私有
    private MagicBoxFactory() {
    }

    // 对外开发获取策略的方法
    public static MagicBox getWantBox(String name){
        MagicBox box = map.get(name);
        return box==null? emptyBox : box;
    }

    // 实际用枚举定义key更好
    private interface BoxKey{
        String CAT = "cat";
        String XIAOMI = "xiaomi";
    }
}
