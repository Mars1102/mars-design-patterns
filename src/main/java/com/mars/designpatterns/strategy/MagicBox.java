package com.mars.designpatterns.strategy;

/**
 * author by: mars
 * Date: 2020/3/10 23:54
 * Description:  魔法盒接口
 */
public interface MagicBox {
    String name = "a";

    void makeBox();

}
