package com.mars.designpatterns.strategy;

/**
 * author by: mars
 * Date: 2020/3/11 0:14
 * Description:
 */
public class ShowMagicBox {

    private MagicBox magicBox;

    public ShowMagicBox(MagicBox magicBox) {
        this.magicBox = magicBox;
    }

    public void showMagixBox(){
        magicBox.makeBox();
    }
}
