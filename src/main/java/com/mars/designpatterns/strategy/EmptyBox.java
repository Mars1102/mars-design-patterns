package com.mars.designpatterns.strategy;

/**
 * author by: mars
 * Date: 2020/3/11 9:31
 * Description:
 */
public class EmptyBox implements MagicBox {

    @Override
    public void makeBox() {
        System.out.println("sorry,是一个空魔盒");
    }
}
