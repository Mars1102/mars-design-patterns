package com.mars.designpatterns.strategy;

/**
 * author by: mars
 * Date: 2020/3/10 23:56
 * Description: 天猫精灵魔法盒
 */
public class CatBox implements MagicBox {

    @Override
    public void makeBox() {
        System.out.println("我是天猫精灵");
    }
}
