package com.mars.designpatterns.pipeline;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author masisi
 * @date 2023/8/29 11:22
 * @Description 传递到管道的上下文
 */
@Data
public class PipelineContext {

    /**
     * 处理开始时间
     */
    private LocalDateTime startTime;
    /**
     * 处理结束时间
     */
    private LocalDateTime endTime;

    /**
     * 获取数据名称
     */
    public String getName(){
        return this.getClass().getSimpleName();
    }
}
