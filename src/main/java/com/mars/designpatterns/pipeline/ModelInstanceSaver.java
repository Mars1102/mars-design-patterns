package com.mars.designpatterns.pipeline;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author masisi
 * @date 2023/8/29 11:34
 * @Description 保存模型管道
 */
@Slf4j
@Service
public class ModelInstanceSaver implements ContextHandler<InstanceBuildContext> {

    @Override
    public boolean handle(InstanceBuildContext context) {
        log.info("--保存模型实例到相关DB表--");

        // 假装保存模型实例

        return true;
    }
}
