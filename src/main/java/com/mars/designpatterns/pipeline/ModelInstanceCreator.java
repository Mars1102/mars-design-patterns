package com.mars.designpatterns.pipeline;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author masisi
 * @date 2023/8/29 11:32
 * @Description 创建模式实例
 */
@Slf4j
@Service
public class ModelInstanceCreator implements ContextHandler<InstanceBuildContext> {

    @Override
    public boolean handle(InstanceBuildContext context) {
        log.info("--根据输入数据创建模型实例--");

        // 假装创建模型实例

        return true;
    }
}
