package com.mars.designpatterns.pipeline;

/**
 * @author masisi
 * @date 2023/8/29 11:25
 * @Description 管道上下问处理器
 */
public interface ContextHandler <T extends PipelineContext> {

    /**
     * 处理输入的上下文数据
     * @param context
     * @return 返回true，表示由下一个handle继续处理
     *         返回false,表示不再向下处理
     */
    boolean handle(T context);
}
