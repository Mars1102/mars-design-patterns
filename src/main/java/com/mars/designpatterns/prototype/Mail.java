package com.mars.designpatterns.prototype;

/**
 * author by: mars
 * Date: 2022/2/6 17:39
 * Description:原型模型
 * 注意：clone的对象不会调用new对象的构造方法
 */
public class Mail implements Cloneable{
    private String name;
    private String address;
    private String content;

    public Mail() {
        System.out.println("execute mail constructor...");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Mail{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", content='" + content + '\'' +
                ", object=" + super.toString() +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        System.out.println("execute clone method...");
        return super.clone();
    }
}
