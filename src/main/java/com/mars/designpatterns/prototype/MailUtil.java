package com.mars.designpatterns.prototype;

import java.text.MessageFormat;

/**
 * author by: mars
 * Date: 2022/2/6 17:38
 * Description: 原型模型
 */
public class MailUtil {

    public static void sendMsg(Mail mail){
        String outContent = "向{0}同学,邮件地址为:{1},发送内容Wie:{2},成功";
        System.out.println(MessageFormat.format(outContent,mail.getName(),mail.getAddress(),mail.getContent()));
    }

    public static void saveOriginMail(Mail mail){
        System.out.println("存储原始mail:"+mail.getContent());
    }
}
