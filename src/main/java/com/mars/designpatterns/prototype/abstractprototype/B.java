package com.mars.designpatterns.prototype.abstractprototype;

/**
 * author by: mars
 * Date: 2022/2/7 9:30
 * Description:不推荐使用该方式
 */
public class B extends A {

    public static void main(String[] args) throws CloneNotSupportedException {
        B b = new B();
        b.clone();
    }
}
