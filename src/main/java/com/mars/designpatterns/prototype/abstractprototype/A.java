package com.mars.designpatterns.prototype.abstractprototype;

/**
 * author by: mars
 * Date: 2022/2/7 9:29
 * Description: 不推荐使用该方式
 */
public abstract class A implements Cloneable{

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
