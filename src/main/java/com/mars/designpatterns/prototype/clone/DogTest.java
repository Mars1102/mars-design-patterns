package com.mars.designpatterns.prototype.clone;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * author by: mars
 * Date: 2022/2/7 18:08
 * Description:
 */
public class DogTest {
    public static void main(String[] args) throws CloneNotSupportedException {
        List<Object> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        Dog dog1 = new Dog("dobbi", list1);
        Dog dog2 = (Dog)dog1.clone();
        System.out.println(dog1);
        System.out.println(dog2);
        dog1.getList().add(3);
        System.out.println(dog1);
        System.out.println(dog2);
    }
}
