package com.mars.designpatterns.prototype.clone;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * author by: mars
 * Date: 2022/2/7 9:31
 * Description:
 */
public class Dog implements Cloneable {
    private String name;
    private List<Object> list;

    public Dog(String name, List<Object> list) {
        this.name = name;
        this.list = list;
    }

    public List<Object> getList() {
        return list;
    }

    public void setList(List<Object> list) {
        this.list = list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", list=" + list +
                '}' + super.toString();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        // 深克隆
        Dog dog = (Dog)super.clone();
        dog.list = new ArrayList<>(this.list);
        return dog;
        //return super.clone();
    }
}
