package com.mars.designpatterns.prototype;

/**
 * author by: mars
 * Date: 2022/2/6 17:51
 * Description:原型模型
 * 注意：clone的对象不会调用new对象的构造方法
 */
public class MailTest {
    public static void main(String[] args) throws CloneNotSupportedException {
        Mail originMail = new Mail();
        originMail.setContent("初始模板");

        for (int i = 1; i <= 5; i++) {
            Mail cloneMail = (Mail) originMail.clone();
            cloneMail.setName("小红"+i);
            cloneMail.setAddress("天津市第" + i + "大街");
            cloneMail.setContent("恭喜你，获得字节跳动第" + i + "个工作名额");
            System.out.println(cloneMail.toString());
            MailUtil.sendMsg(cloneMail);
        }
        // 发完最后一份邮件，输出原始邮件模板内容
        MailUtil.saveOriginMail(originMail);
    }
}
