package com.mars.designpatterns.proxy;

/**
 * author by: mars
 * Date: 2020/3/8 16:47
 * Description:
 */
public class OrderDaoImpl implements IOrderDao {

    @Override
    public int insert(Order order) {
        System.out.println("Dao层添加Order成功");
        return 1;
    }
}
