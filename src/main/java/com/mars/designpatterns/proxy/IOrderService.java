package com.mars.designpatterns.proxy;

/**
 * author by: mars
 * Date: 2020/3/8 16:46
 * Description:
 */
public interface IOrderService {

    int saveOrder(Order order);
}
