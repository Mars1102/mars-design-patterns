package com.mars.designpatterns.proxy;

/**
 * author by: mars
 * Date: 2020/3/8 17:08
 * Description:
 */
public class OrderServiceImpl implements IOrderService {

    private IOrderDao iOrderDao;

    @Override
    public int saveOrder(Order order) {
        // spring中需注入，这里直接new代替
        iOrderDao = new OrderDaoImpl();
        System.out.println("Service层调用Dao层添加order");
        return iOrderDao.insert(order);
    }
}
