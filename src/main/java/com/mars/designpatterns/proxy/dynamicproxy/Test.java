package com.mars.designpatterns.proxy.dynamicproxy;

import com.mars.designpatterns.proxy.IOrderService;
import com.mars.designpatterns.proxy.Order;
import com.mars.designpatterns.proxy.OrderServiceImpl;
import com.mars.designpatterns.proxy.staticproxy.OrderServiceStaticProxy;

/**
 * author by: mars
 * Date: 2020/3/9 0:09
 * Description:
 */
public class Test {

    public static void main(String[] args) {
        Order order = new Order();
        order.setUserId(3);
        // 动态代理，直接代理接口
        IOrderService proxy = (IOrderService)new OrderServiceDynamicProxy(new OrderServiceImpl()).bind();
        proxy.saveOrder(order);
    }
}
