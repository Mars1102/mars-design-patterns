package com.mars.designpatterns.proxy.db;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;


/**
 * author by: mars
 * Date: 2020/3/8 17:37
 * Description:
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        return DataSourceContextHolder.getDBType();//返回值表示，当前线程是哪一个db
    }
}
