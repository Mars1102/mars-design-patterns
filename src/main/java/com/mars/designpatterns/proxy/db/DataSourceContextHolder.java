package com.mars.designpatterns.proxy.db;

/**
 * author by: mars
 * Date: 2020/3/8 17:43
 * Description:
 */
public class DataSourceContextHolder {

    private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<String>();

    public static String getDBType(){
       return (String)CONTEXT_HOLDER.get();
    }

    public static void setDBType(String dbType){
        CONTEXT_HOLDER.set(dbType);
    }

    public static void remove(){
        CONTEXT_HOLDER.remove();
    }
}
