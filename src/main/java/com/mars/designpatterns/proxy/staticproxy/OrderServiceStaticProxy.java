package com.mars.designpatterns.proxy.staticproxy;

import com.mars.designpatterns.proxy.IOrderService;
import com.mars.designpatterns.proxy.Order;
import com.mars.designpatterns.proxy.OrderServiceImpl;
import com.mars.designpatterns.proxy.db.DataSourceContextHolder;

/**
 * author by: mars
 * Date: 2020/3/8 17:14
 * Description:静态代理
 */
public class OrderServiceStaticProxy {

    private IOrderService iOrderService;//目标对象

    public int saveOrder(Order order){
        beforeMethod();
        iOrderService = new OrderServiceImpl();
        int userId = order.getUserId();
        int router = userId % 2;
        System.out.println("静态代理分配到 【DB"+router+"】处理数据");
        // 设置datasource
        DataSourceContextHolder.setDBType("db"+ String.valueOf(router));
        afterMethod();
        return iOrderService.saveOrder(order);
    }

    private void afterMethod() {
        System.out.println("静态代理after code");
    }

    private void beforeMethod() {
        System.out.println("静态代理before code");
    }
}
