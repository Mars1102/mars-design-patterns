package com.mars.designpatterns.proxy.staticproxy;

import com.mars.designpatterns.proxy.Order;

/**
 * author by: mars
 * Date: 2020/3/8 21:53
 * Description:
 */
public class Test {

    public static void main(String[] args) {
        Order order = new Order();
        order.setUserId(3);
        OrderServiceStaticProxy proxy = new OrderServiceStaticProxy();
        proxy.saveOrder(order);
    }


}
