package com.mars.designpatterns.proxy;

/**
 * author by: mars
 * Date: 2020/3/8 16:46
 * Description:
 */
public interface IOrderDao {

    int insert(Order order);
}
