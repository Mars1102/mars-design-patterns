package com.mars.designpatterns.proxy.cglib;

/**
 * author by: mars
 * Date: 2020/3/10 9:18
 * Description:
 */
public class HelloServiceImpl {

    void sayHi(String name){
        System.out.println("hello,"+name);
    }

    void running(String name){
        System.out.println("i running "+name);
    }
}
