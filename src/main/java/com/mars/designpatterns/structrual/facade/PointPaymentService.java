package com.mars.designpatterns.structrual.facade;

/**
 * author by: mars
 * Date: 2022/2/8 10:12
 * Description:外观模式
 * - 子系统：兑换支付系统
 */
public class PointPaymentService {

    public boolean pay(PointsGift pointsGift) {
        System.out.println("兑换" + pointsGift.getName() + "  礼品成功");
        return true;
    }

    public String getPayNo(PointsGift pointsGift) {
        String no = "124";
        System.out.println("兑换" + pointsGift.getName() + "  成功，并产生订单号：" + no);
        return no;
    }
}
