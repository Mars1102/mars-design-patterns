package com.mars.designpatterns.structrual.facade;

/**
 * author by: mars
 * Date: 2022/2/8 10:12
 * Description:外观模式
 * - 子系统：校验库存系统
 */
public class QualifyService {

    public boolean isAvaliable(PointsGift pointsGift) {
        System.out.println("校验" + pointsGift.getName() + " 库存有剩余,可以进行兑换");
        return true;
    }
}
