package com.mars.designpatterns.structrual.facade;

/**
 * author by: mars
 * Date: 2022/2/8 10:12
 * Description:外观模式
 * - 子系统：物流系统
 */
public class ShippingService {

    public boolean shipGift(PointsGift pointsGift) {
        System.out.println("准备对" + pointsGift.getName() + "  礼品进行出仓操作，开始物流运输");
        return true;
    }
}
