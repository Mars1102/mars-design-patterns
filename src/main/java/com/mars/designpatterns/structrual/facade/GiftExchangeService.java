package com.mars.designpatterns.structrual.facade;

/**
 * author by: mars
 * Date: 2022/2/8 10:18
 * Description: 外观模式
 * 兑换礼品 门面类，与子系统进行交互
 */
public class GiftExchangeService {
    private PointPaymentService pointPaymentService = new PointPaymentService();
    private QualifyService qualifyService = new QualifyService();
    private ShippingService shippingService = new ShippingService();

    public void exchangeGift(PointsGift pointsGift){
        if (qualifyService.isAvaliable(pointsGift)) {
            if (pointPaymentService.pay(pointsGift)){
                pointPaymentService.getPayNo(pointsGift);
                if (shippingService.shipGift(pointsGift)){
                    System.out.println("用户完成 " + pointsGift.getName() + " 的兑换");
                }
            }
        }
    }
}
