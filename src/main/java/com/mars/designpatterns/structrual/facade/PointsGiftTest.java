package com.mars.designpatterns.structrual.facade;

/**
 * author by: mars
 * Date: 2022/2/8 10:22
 * Description: 外观模式-直接调用门面类或接口，不与子系统进行交互，保证迪米特法则
 * -扩展，如果子系统增加，会破坏门面代码层的开闭原则，建议创建门面抽象类，进行扩展
 */
public class PointsGiftTest {

    public static void main(String[] args) {
        PointsGift pointsGift = new PointsGift(" 冰墩墩");
        GiftExchangeService giftExchangeService = new GiftExchangeService();
        giftExchangeService.exchangeGift(pointsGift);
    }
}
