package com.mars.designpatterns.structrual.facade;

/**
 * author by: mars
 * Date: 2022/2/8 10:00
 * Description: 外观模式
 */
public class PointsGift {

    private String name;

    public PointsGift(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
