package com.mars.designpatterns.structrual.decorator.v2;

/**
 * author by: mars
 * Date: 2022/2/9 11:12
 * Description: 奶油装饰者实体
 */
public class CreamDecorator extends AbstractDecorator{

    public CreamDecorator(AbstractBirthdayCake abstractBirthdayCake) {
        super(abstractBirthdayCake);
    }

    @Override
    protected String desc() {
        return super.desc() + " 加一坨奶油";
    }

    @Override
    protected float price() {
        return super.price() + 10;
    }
}
