package com.mars.designpatterns.structrual.decorator.v2;

/**
 * author by: mars
 * Date: 2022/2/9 11:08
 * Description: 蛋糕装饰者抽象类
 * 将实体蛋糕 和 抽象蛋糕装饰者关联起来，需要借助实体蛋糕的 父类
 */
public abstract class AbstractDecorator extends AbstractBirthdayCake{

    private AbstractBirthdayCake abstractBirthdayCake;

    public AbstractDecorator(AbstractBirthdayCake abstractBirthdayCake) {
        this.abstractBirthdayCake = abstractBirthdayCake;
    }

    @Override
    protected String desc() {
        return abstractBirthdayCake.desc();
    }

    @Override
    protected float price() {
        return abstractBirthdayCake.price();
    }
}
