package com.mars.designpatterns.structrual.decorator.v2;

/**
 * author by: mars
 * Date: 2022/2/9 11:12
 * Description:草莓装饰者实体
 */
public class StrawberryDecorator extends AbstractDecorator{

    public StrawberryDecorator(AbstractBirthdayCake abstractBirthdayCake) {
        super(abstractBirthdayCake);
    }

    @Override
    protected String desc() {
        return super.desc() + " 加5颗草莓";
    }

    @Override
    protected float price() {
        return super.price() + 20;
    }
}
