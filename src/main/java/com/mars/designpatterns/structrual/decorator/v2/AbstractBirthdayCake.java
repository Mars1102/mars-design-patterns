package com.mars.designpatterns.structrual.decorator.v2;

/**
 * author by: mars
 * Date: 2022/2/9 11:04
 * Description: 蛋糕主体抽象类
 */
public abstract class AbstractBirthdayCake {

    protected abstract String desc();
    protected abstract float price();

}
