package com.mars.designpatterns.structrual.decorator.v1;

/**
 * author by: mars
 * Date: 2022/2/9 9:13
 * Description:
 */
public class Vegetable extends Food {

    private Food food;

    public Vegetable(Food food) {
        this.food = food;
    }

    @Override
    public String make() {
        return food.make() + " 蔬菜 " ;
    }
}
