package com.mars.designpatterns.structrual.decorator.v2;

/**
 * author by: mars
 * Date: 2022/2/9 11:14
 * Description:
 */
public class BirthdayCakeTest {

    public static void main(String[] args) {
        AbstractBirthdayCake birthdayCake;
        // 先来一个蛋糕基础款
        birthdayCake = new BirthdayCake();
        // 再来装饰3托奶油
        birthdayCake  = new CreamDecorator(birthdayCake);
        birthdayCake  = new CreamDecorator(birthdayCake);
        birthdayCake  = new CreamDecorator(birthdayCake);
        // 再来 15颗草莓
        birthdayCake  = new StrawberryDecorator(birthdayCake);
        birthdayCake  = new StrawberryDecorator(birthdayCake);
        birthdayCake   = new StrawberryDecorator(birthdayCake);

        System.out.println(birthdayCake.desc());
        System.out.println(birthdayCake.price());

    }
}
