package com.mars.designpatterns.structrual.decorator.v1;

/**
 * author by: mars
 * Date: 2022/2/9 9:12
 * Description:
 */
public class Food {

    private String foodName;

    public Food() {
    }

    public Food(String foodName) {
        this.foodName = foodName;
    }

    public String  make(){
        return foodName;
    }
}
