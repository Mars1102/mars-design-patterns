package com.mars.designpatterns.structrual.decorator.v2;

/**
 * author by: mars
 * Date: 2022/2/9 11:07
 * Description: 蛋糕实体
 */
public class BirthdayCake extends AbstractBirthdayCake{

    @Override
    protected String desc() {
        return "蛋糕";
    }

    @Override
    protected float price() {
        return 100;
    }
}
