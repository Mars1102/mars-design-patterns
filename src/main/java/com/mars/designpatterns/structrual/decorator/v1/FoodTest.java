package com.mars.designpatterns.structrual.decorator.v1;

/**
 * author by: mars
 * Date: 2022/2/9 9:18
 * Description:
 */
public class FoodTest {
    public static void main(String[] args) {
        Food food = new Bread(new Egg(new Vegetable(new Food(" 培根 "))));
        System.out.println(food.make());
    }
}
