package com.mars.designpatterns.structrual.composite;

/**
 * author by: mars
 * Date: 2022/2/14 9:40
 * Description: 组合模式：课程+目录 公用的组件抽象类
 *
 * 叶子结点和组合节点都要实现相同接口或者继承相同的抽象类
 *
 */
public abstract class CatalogComponent {

    public void add(CatalogComponent catalogComponent){
        throw new UnsupportedOperationException("不支持添加操作");
    }

    public void remove(CatalogComponent catalogComponent){
        throw new UnsupportedOperationException("不支持删除操作");
    }

    public String getName(CatalogComponent catalogComponent){
        throw new UnsupportedOperationException("不支持获取名称操作");
    }

    public double getPrice(CatalogComponent catalogComponent){
        throw new UnsupportedOperationException("不支持获取价格操作");
    }

    public void print(){
        throw new UnsupportedOperationException("不支持打印操作");
    }
}
