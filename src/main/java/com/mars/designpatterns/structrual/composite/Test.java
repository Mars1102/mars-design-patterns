package com.mars.designpatterns.structrual.composite;

/**
 * author by: mars
 * Date: 2022/2/14 9:53
 * Description:组合模式:
 */
public class Test {

    public static void main(String[] args) {

        CatalogComponent java = new Course("Java架构师课程",599);
        CatalogComponent go = new Course("Go架构师课程",499);

        CatalogComponent catalog = new Catalog("架构师培养目录",2);
        catalog.add(java);
        catalog.add(go);
        CatalogComponent catalog2 = new Catalog("网络编程师培养目录",1);
        catalog2.add(new Course("PMP课程",100));
        catalog2.add(new Course("产品经理课程",101));
        catalog2.add(catalog);

        catalog2.print();
    }
}
