package com.mars.designpatterns.structrual.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * author by: mars
 * Date: 2022/2/14 9:48
 * Description:  组合模式:目录
 */
public class Catalog extends CatalogComponent{

    private String catalogName;
    private Integer level;
    private List<CatalogComponent> items = new ArrayList<>();

    public Catalog(String catalogName,Integer level) {
        this.catalogName = catalogName;
        this.level = level;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    @Override
    public void add(CatalogComponent catalogComponent) {
        items.add(catalogComponent);
    }

    @Override
    public void remove(CatalogComponent catalogComponent) {
        items.remove(catalogComponent);
    }

    @Override
    public void print() {
        System.out.println(this.catalogName);
        for (CatalogComponent item : items) {
            if (level != null) {
                for (int i = 0; i < level; i++) {
                    System.out.print("   ");
                }
            }
            item.print();
        }
    }
}
