package com.mars.designpatterns.structrual.bridge;

/**
 * author by: mars
 * Date: 2022/2/15 10:07
 * Description:桥接模式 - 活期账号实现
 */
public class HuoqiAccount implements Account {

    @Override
    public Account openAccount() {
        System.out.println("打开活期账号");
        return new HuoqiAccount();
    }

    @Override
    public void showAccountType() {
        System.out.println("这是一个活期账号类型");
    }
}
