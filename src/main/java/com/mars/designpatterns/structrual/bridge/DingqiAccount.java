package com.mars.designpatterns.structrual.bridge;

/**
 * author by: mars
 * Date: 2022/2/15 10:07
 * Description:桥接模式 - 定期账号实现
 */
public class DingqiAccount implements Account {

    @Override
    public Account openAccount() {
        System.out.println("打开定期账号");
        return new DingqiAccount();
    }

    @Override
    public void showAccountType() {
        System.out.println("这是一个定期账号类型");
    }
}
