package com.mars.designpatterns.structrual.bridge;

/**
 * author by: mars
 * Date: 2022/2/15 10:12
 * Description:
 */
public class ABCBank extends Bank {

    public ABCBank(Account account) {
        super(account);
    }

    @Override
    Account openAccount() {
        System.out.println("打开农业银行账号");
        // 委托接口代理 该打开方法
        account.openAccount();
        return account;
    }

}
