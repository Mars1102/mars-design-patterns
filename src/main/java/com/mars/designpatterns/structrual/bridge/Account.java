package com.mars.designpatterns.structrual.bridge;

/**
 * author by: mars
 * Date: 2022/2/15 10:06
 * Description: 桥接模式 - 账号接口
 */
public interface Account {

    Account openAccount();

    void showAccountType();

}
