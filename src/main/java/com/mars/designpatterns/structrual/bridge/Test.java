package com.mars.designpatterns.structrual.bridge;

/**
 * author by: mars
 * Date: 2022/2/15 10:25
 * Description:
 */
public class Test {

    public static void main(String[] args) {
        Bank abcBank = new ABCBank(new DingqiAccount());
        Account abcCount = abcBank.openAccount();
        abcCount.showAccountType();

        Bank icbcBank = new ICBCBank(new HuoqiAccount());
        Account icbcCount = icbcBank.openAccount();
        icbcCount.showAccountType();

    }
}
