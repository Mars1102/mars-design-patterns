package com.mars.designpatterns.structrual.bridge;

/**
 * author by: mars
 * Date: 2022/2/15 10:10
 * Description: 桥接模式 - 银行抽象类
 */
public abstract class Bank {
    public Bank(Account account) {
        this.account = account;
    }

    // 定义为protected 类型，让子类可以使用
    protected Account account;

    abstract Account openAccount();
}
