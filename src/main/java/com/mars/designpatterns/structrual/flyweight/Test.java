package com.mars.designpatterns.structrual.flyweight;

/**
 * author by: mars
 * Date: 2022/2/11 9:37
 * Description:
 */
public class Test {

    private static final String depts[] = {"PD","QA","QT","PM"};
    public static void main(String[] args) {
        for (int i = 0; i < 10 ; i++) {
            int a = (int)Math.random() * depts.length;
            String dept = depts[(int)(Math.random() * depts.length)];
            ManagerEmployee managerEmployee = (ManagerEmployee)EmployeeFactory.getManage(dept);
            managerEmployee.report();
        }
    }
}
