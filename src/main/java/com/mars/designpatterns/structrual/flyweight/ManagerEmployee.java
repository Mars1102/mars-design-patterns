package com.mars.designpatterns.structrual.flyweight;

/**
 * author by: mars
 * Date: 2022/2/11 9:25
 * Description: 享元模式：减少对象创建 和内存占用
 */
public class ManagerEmployee implements Employee{

    private String department;
    private String reportContent;

    public ManagerEmployee(String department) {
        this.department = department;
    }

    @Override
    public void report() {
        System.out.println(reportContent);
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getReportContent() {
        return reportContent;
    }

    public void setReportContent(String reportContent) {
        this.reportContent = reportContent;
    }
}
