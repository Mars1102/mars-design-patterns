package com.mars.designpatterns.structrual.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * author by: mars
 * Date: 2022/2/11 9:28
 * Description: 享元模式：减少对象创建 和内存占用
 * 员工工厂类
 */
public class EmployeeFactory {

    private static Map<String,Employee> map = new HashMap<>();

    public static Employee getManage(String department){
        ManagerEmployee manager = (ManagerEmployee) map.get(department);
        if ( null == manager) {
            manager = new ManagerEmployee(department);
            System.out.println("创建" + department + " 部门经理");
            manager.setReportContent("此次报告内容由" + department + "部门来做");
            System.out.println("创建报告");
            map.put(department,manager);

        }
        return manager;
    }
}


