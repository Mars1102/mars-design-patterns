package com.mars.designpatterns.structrual.flyweight;

/**
 * author by: mars
 * Date: 2022/2/11 9:24
 * Description: 享元模式：减少对象创建 和内存占用
 */
public interface Employee {

    // 做报告
    void report();
}
