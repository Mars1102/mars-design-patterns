package com.mars.designpatterns.structrual.adapter.objectadaptor;


/**
 * author by: mars
 * Date: 2022/2/10 10:55
 * Description: 适配器-对象适配
 */
public class ConcreteTarget implements Target {
    @Override
    public void request() {
        System.out.println("对象适配-具体的目标方法");
    }
}
