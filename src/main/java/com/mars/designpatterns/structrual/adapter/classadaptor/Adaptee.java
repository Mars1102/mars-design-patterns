package com.mars.designpatterns.structrual.adapter.classadaptor;

/**
 * author by: mars
 * Date: 2022/2/10 10:53
 * Description:
 */
public class Adaptee {

    public void adapteeInfo(){
        System.out.println("被适配的方法");
    }
}
