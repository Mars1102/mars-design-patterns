package com.mars.designpatterns.structrual.adapter.objectadaptor;

/**
 * author by: mars
 * Date: 2022/2/10 10:55
 * Description: 适配器-对象适配
 */
public interface Target {

    void request();
}
