package com.mars.designpatterns.structrual.adapter.classadaptor;

/**
 * author by: mars
 * Date: 2022/2/10 10:55
 * Description:
 */
public class ConcreteTarget implements Target {

    @Override
    public void request() {
        System.out.println("具体的目标方法");
    }
}
