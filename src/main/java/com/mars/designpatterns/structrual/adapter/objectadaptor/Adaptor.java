package com.mars.designpatterns.structrual.adapter.objectadaptor;


/**
 * author by: mars
 * Date: 2022/2/10 10:57
 * Description: 适配器-对象适配
 *  组合 适配对象
 */
public class Adaptor implements Target {
    public Adaptee adaptee2 = new Adaptee();
    @Override
    public void request() {
        adaptee2.adapteeInfo();
    }

}
