package com.mars.designpatterns.structrual.adapter.classadaptor;

import com.mars.designpatterns.structrual.adapter.objectadaptor.Adaptor;
import com.mars.designpatterns.structrual.adapter.objectadaptor.ConcreteTarget;

/**
 * author by: mars
 * Date: 2022/2/10 11:03
 * Description: 对象适配：都是调用统一接口，
 * 适配对象 实现 目标接口
 */
public class ClassAdaptorTest {

    public static void main(String[] args) {
        // 具体目标
        com.mars.designpatterns.structrual.adapter.objectadaptor.Target target = new ConcreteTarget();
        target.request();
        // 适配后调用具体目标
        com.mars.designpatterns.structrual.adapter.objectadaptor.Target adaptor = new Adaptor();
        adaptor.request();
    }
}
