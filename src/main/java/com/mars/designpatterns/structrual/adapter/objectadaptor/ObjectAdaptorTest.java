package com.mars.designpatterns.structrual.adapter.objectadaptor;

import com.mars.designpatterns.structrual.adapter.objectadaptor.Target;

/**
 * author by: mars
 * Date: 2022/2/10 11:03
 * Description: 类适配：都是调用统一接口
 */
public class ObjectAdaptorTest {

    public static void main(String[] args) {
        // 具体目标
        Target target = new ConcreteTarget();
        target.request();
        // 适配后调用具体目标
        Target adaptor = new Adaptor();
        adaptor.request();
    }
}
