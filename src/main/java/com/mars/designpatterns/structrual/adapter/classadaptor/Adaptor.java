package com.mars.designpatterns.structrual.adapter.classadaptor;

/**
 * author by: mars
 * Date: 2022/2/10 10:57
 * Description: 类适配-Adaptor 为适配器，继承被适配的方法 并实现目标结构
 */
public class Adaptor extends Adaptee implements Target {

    @Override
    public void request() {
        super.adapteeInfo();
    }

}
