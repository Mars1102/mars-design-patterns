package com.mars.designpatterns.structrual.adapter.classadaptor;

/**
 * author by: mars
 * Date: 2022/2/10 10:55
 * Description:
 */
public interface Target {

    void request();
}
