package com.mars.designpatterns.structrual.adapter.objectadaptor;

/**
 * author by: mars
 * Date: 2022/2/10 10:53
 * Description:适配器-对象适配
 */
public class Adaptee {
    public void adapteeInfo(){
        System.out.println("对象适配-被适配的方法");
    }
}
