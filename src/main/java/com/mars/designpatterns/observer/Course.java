package com.mars.designpatterns.observer;

import java.util.Observable;

/**
 * @Author mars
 * @Description 使用jdk自带的观察者 extends Observable,使用的Course变为被观察者
 * @Date
 * @return
 */
public class Course extends Observable {

    private String courseName;

    public Course(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    // 生成一个问题
    public void produceQuestion(Course course,Question question){
        System.out.println(question.getUserName()+"在" + course.courseName + "里提了一个问题");
        // 设置改变状态
        setChanged();
        // 设置通知（将问题通知给观察者）
        notifyObservers(question);
    }
}
