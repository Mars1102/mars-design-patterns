package com.mars.designpatterns.observer;

import java.math.BigDecimal;

public class ObserverTest {
    public static void main(String[] args) {
        Course course = new Course("【测试课程】");
        Teacher teacher = new Teacher("生物");

        // 给课程添加老师观察者
        course.addObserver(teacher);
        Question question = new Question("lili","火星上有生命吗");
        course.produceQuestion(course,question);

        for (int i=0;i<3;i++) {
            BigDecimal a = new BigDecimal("1");
            BigDecimal b = new BigDecimal("2");
            System.out.println(a.add(b));
        }
    }
}
