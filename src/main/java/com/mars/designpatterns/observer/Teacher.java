package com.mars.designpatterns.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * @Author mars
 * @Description 观察者，implement Observer
 * 关键方法：是 extends Observable 中的notification方法调用了 Observer.update()方法
 * @Date
 * @return
 */
public class Teacher implements Observer {

    private String teacherName;

    public Teacher(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    @Override
    public void update(Observable o, Object arg) {
        Course course = (Course) o;
        Question question = (Question) arg;
        System.out.println(teacherName+"老师的"+course.getCourseName()+"课程接收到一个"+question.getUserName()+"提交的问题"+question.getQuestionContent());
    }
}
