package com.mars.designpatterns.observer;

public class Question {

    private String userName;
    private String questionContent;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public Question(String userName, String questionContent) {
        this.userName = userName;
        this.questionContent = questionContent;
    }
}
