package com.mars.designpatterns.behavioral.command;

/**
 * author by: mars
 * Date: 2022/2/17 9:54
 * Description:
 */
public class Test {

    public static void main(String[] args) {
        CourseVideo courseVideo = new CourseVideo("Java架构师体系课");
        // 命令
        Command openCmd = new OpenCommand(courseVideo);
        Command closeCmd = new CloseCommand(courseVideo);

        Staff staff = new Staff();
        staff.addCommand(openCmd);
        staff.addCommand(closeCmd);

        staff.executeCommands();
    }
}
