package com.mars.designpatterns.behavioral.command;

import java.util.ArrayList;
import java.util.List;

/**
 * author by: mars
 * Date: 2022/2/17 9:53
 * Description:
 */
public class Staff {

    public List<Command> commands = new ArrayList<>();

    public void addCommand(Command command){
        commands.add(command);
    }

    public void executeCommands(){
        for (Command command : commands) {
            command.execute();
        }
        commands.clear();
    }
}
