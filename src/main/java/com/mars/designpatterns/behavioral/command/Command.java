package com.mars.designpatterns.behavioral.command;

/**
 * author by: mars
 * Date: 2022/2/17 9:42
 * Description:  命令模式
 */
public interface Command {

    void execute();
}
