package com.mars.designpatterns.behavioral.command;

/**
 * author by: mars
 * Date: 2022/2/17 9:47
 * Description:
 */
public class CloseCommand implements Command {

    private CourseVideo courseVideo;

    public CloseCommand(CourseVideo courseVideo) {
        this.courseVideo = courseVideo;
    }

    @Override
    public void execute() {
        courseVideo.close();
    }
}
