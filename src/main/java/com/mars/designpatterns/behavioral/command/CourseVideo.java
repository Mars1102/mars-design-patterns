package com.mars.designpatterns.behavioral.command;

/**
 * author by: mars
 * Date: 2022/2/17 9:46
 * Description:
 */
public class CourseVideo {

    private String name;

    public CourseVideo(String name) {
        this.name = name;
    }

    public void open(){
        System.out.println("打开 " + name + " 课程");
    }

    public void close(){
        System.out.println("关闭 " + name + " 课程");
    }
}
