package com.mars.designpatterns.singleton;

/**
 * @Author mars
 * @Description 单例模式：
 * 1.栗子a：双重检查懒加载
 *
 * @Date
 * @return
 */
public class DoubleCheckLazySingleton {

    private volatile static DoubleCheckLazySingleton lazySingleton = null; //加入volatile防止内存中的指令重排序，导致在多线程情况下的产生的异常

    private DoubleCheckLazySingleton() {}

    public static DoubleCheckLazySingleton getInstance(){
        if(lazySingleton==null) {
            synchronized (DoubleCheckLazySingleton.class) {
                if (lazySingleton==null) {
                    lazySingleton = new DoubleCheckLazySingleton();
                }
            }
        }
        return lazySingleton;
    }
}
