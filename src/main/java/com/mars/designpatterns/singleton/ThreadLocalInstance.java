package com.mars.designpatterns.singleton;

/**
 * author by: mars
 * Date: 2020/3/1 16:37
 * Description: ThreadLocal的单例（带引号的单例模式）
 * 存在的隐患：使用不当会导致内存泄漏，注意使用方式
 */
public class ThreadLocalInstance {

    private static final ThreadLocal<ThreadLocalInstance> instance = new ThreadLocal(){
        @Override
        protected Object initialValue() {
            // 重载创建 ThreadLocalInstance对象
            return new ThreadLocalInstance();
        }
    };

    // 私有构造器
    private ThreadLocalInstance() {
    }

    //  对外暴露一个获取实例的方法
    public static ThreadLocalInstance getInstance(){
        return instance.get();
    }

}
