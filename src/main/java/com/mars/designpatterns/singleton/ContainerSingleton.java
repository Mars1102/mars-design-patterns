package com.mars.designpatterns.singleton;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * author by: mars
 * Date: 2020/2/2 17:24
 * Description: 容器单例
 */
public class ContainerSingleton {

    private static Map<String,Object> map = new HashMap <String,Object>();

    public ContainerSingleton() {
    }

    public static void putInstance(String key, Object instance){
        if(StringUtils.isNotBlank(key) && instance !=null){
            if(!map.containsKey(key)){
                map.put(key, instance);
            }
        }
    }

    public static Object getInstance(String key){
        return map.get(key);
    }

}
