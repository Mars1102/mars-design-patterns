package com.mars.designpatterns.singleton;

/**
 * @Author mars
 * @Description 静态内部类的单例模式
 * 》 初始化一个类包括：执行这个类的静态初始化，初始化类中的静态变量
 * 》 5中情况：
 *    a.首次发生时一个类会立刻初始化（包含interface接口）
 *    b.栗子 A 类,会被初始化的5中情况
 *      》 A类型的实例被创建
 *      》 A类中声明的静态方法被调用
 *      》 A类中声明的静态成员被赋值
 *      》 A类中声明的静态成员被使用，并且该成员不是常量成员
 *      》 A类如果是一个顶级类，且类中存在嵌套的断言语句
 * @Date
 * @return
 */
public class StaticInnerClassSingleton {

    private static class StaticInner{// private 静态内部类
        private static StaticInnerClassSingleton singleton = new StaticInnerClassSingleton();

    }

    public static StaticInnerClassSingleton getInstance(){
        return StaticInner.singleton;
    }
}
