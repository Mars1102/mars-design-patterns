package com.mars.designpatterns.singleton;

import java.io.Serializable;

/**
 * @Author mars
 * @Description 饿汉式单例
 * 1.特点：类加载的时候就进行初始化操作，避免了线程同步问题，但是没有延迟加载的效果
 * 2.栗子：
 * 》 第一种： 直接定义自己类型的属性，并初始化
 * 》 第二种：使用静态代码块
 *
 * @Date
 * @return
 */
public class HungrySingleton implements Serializable,Cloneable {
//    第一种
//    private static final HungrySingleton hungry = new HungrySingleton();
//    第二种
    private static final HungrySingleton hungry;

    static {
        hungry = new HungrySingleton();
    }

    private HungrySingleton() {
        if(hungry!=null){
            throw new RuntimeException("单例构造器禁止反射调用");
        }

    }

    public static HungrySingleton getInstance(){
        return hungry;
    }

    // 反射会调用的方法
    // 通过读写流转换后，会报错，提示序列化的报错
    // 解决报错：
    //   1.类序列化，但是对象不是同一个
    //   2.利用反射，写readResolve()方法，在单例中要注意破坏序列化和反序列化的情况
    private Object readResolve(){
        return hungry;
    }

    // 破坏单例模式的克隆实现
    @Override
    protected Object clone() throws CloneNotSupportedException {
        //return super.clone();
        // 防止破解单例，直接调用getInstance()返回
        return getInstance();
    }
}
