package com.mars.designpatterns.singleton;

public class ThreadTest implements Runnable {

    @Override
    public void run() {
        // 线程里获取对象
        //LazySingleton lazySingleton = LazySingleton.getInstance();
        //System.out.println(Thread.currentThread().getName() + " " + lazySingleton);
        ContainerSingleton.putInstance("object",new Object());
        Object instance = ContainerSingleton.getInstance("object");
        System.out.println(Thread.currentThread().getName() + " " + instance);
    }
}
