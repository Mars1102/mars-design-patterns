package com.mars.designpatterns.singleton;

/**
 * author by: mars
 * Date: 2020/2/1 10:56
 * Description: 枚举类的单例模式
 * 1.枚举类型自带序列化机制，不会出现多次序列化
 * 2.枚举类中声明方法：
 *
 */
public enum EnumsInstance {

    INSTANCE{
        protected void printTest(){
            System.out.println("mars print test");
        }
    };

    protected abstract void printTest();

    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    // 获取枚举实例
    public static EnumsInstance getInstance(){
        return INSTANCE;
    }

    public static void main(String[] args) {
        EnumsInstance instance = EnumsInstance.getInstance();
        instance.printTest();
    }
}
