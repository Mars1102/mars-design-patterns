package com.mars.ms.aqs;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * author by: mars
 * Date: 2022/3/6 14:24
 * Description:
 */
public class MutexDemo {

    private  final SyncDemo sync = new SyncDemo();

    static class SyncDemo extends AbstractQueuedSynchronizer{

        @Override
        protected boolean tryAcquire(int arg) {
            return compareAndSetState(0,1);
        }

        @Override
        protected boolean tryRelease(int arg) {
            return compareAndSetState(1,0);
        }
    }
}
