package com.mars.ms.sychronized;

import java.util.concurrent.locks.ReentrantLock;

/**
 * author by: mars
 * Date: 2022/2/28 23:43
 * Description: 使用 ReentrantLock 并进行中断操作，实现真正意义的中断
 */
public class ReentLockInterruptDemo1 {

    public static void main(String[] args) throws InterruptedException {
        ReentrantLock lock = new ReentrantLock();
        Thread t = new Thread(()->{
            try {
                lock.lockInterruptibly();
                for (int i = 0; i < 10000 ; i++) {}
                System.out.println("finished");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        });

        t.start();
        // 该中断起作用
        t.interrupt();
        t.join();
    }
}
