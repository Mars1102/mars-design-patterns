package com.mars.ms.sychronized;

/**
 * author by: mars
 * Date: 2022/2/28 23:43
 * Description: Synchronized 锁 配合 wait()/notify()
 */
public class SynchronizedDemo1 {

    public static void main(String[] args) throws InterruptedException {

        Object obj = new Object();
        Thread t1 = new Thread(()->{
            System.out.println("t1 before..");
            try {
                synchronized (obj){
                    obj.wait();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("t1 after..");
        });

        Thread t2 = new Thread(()->{
            System.out.println("t2 before..");
            try {
                synchronized (obj){
                    obj.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("t2 after..");
        });

        t1.start();
        t2.start();

        Thread.sleep(1);
        synchronized (obj){
            obj.notifyAll();
        }
    }
}
