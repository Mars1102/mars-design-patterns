package com.mars.ms.sychronized;

/**
 * author by: mars
 * Date: 2022/2/28 23:43
 * Description: 使用Synchronized 并进行中断操作，无法
 * 实现真正意义的中断
 */
public class SynchronizedNoInterruptDemo1 {

    public static void main(String[] args) throws InterruptedException {
        Object lock = new Object();
        Thread t = new Thread(()->{
           synchronized (lock) {
               for (int i = 0; i < 10000 ; i++) {}
               System.out.println("finished");
           }
        });

        t.start();
        // 该中断未起作用
        t.interrupt();
    }
}
