package com.mars.ms.sychronized;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * author by: mars
 * Date: 2022/2/28 23:43
 * Description: ReentrantLock：配合 condition() + lock() + unlock()
 */
public class SynchronizedDemo2 {

    public static void main(String[] args) throws InterruptedException {
        ReentrantLock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        Thread t1 = new Thread(()->{
            System.out.println("t1 before..");
            try {
                lock.lock();
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
            System.out.println("t1 after..");
        });

        Thread t2 = new Thread(()->{
            System.out.println("t2 before..");
            try {
                lock.lock();
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
            System.out.println("t2 after..");
        });

        t1.start();
        t2.start();

        Thread.sleep(1);
        lock.lock();
        condition.signalAll();
        lock.unlock();
    }
}
