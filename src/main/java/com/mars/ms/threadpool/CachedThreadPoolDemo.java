package com.mars.ms.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * author by: mars
 * Date: 2022/3/19 23:23
 * Description:
 * newCachedThreadPool: 无界线程池，自动回收多余线程功能
 * new ThreadPoolExecutor(
 *          0,                  -- 核心线程数
 *          Integer.MAX_VALUE,  -- 最大线程数
 *          60L, TimeUnit.MILLISECONDS,  -- keeplivetime，空闲线程存活时间
 *          new SynchronousQueue<Runnable>());  -- 工作队列
 */
public class CachedThreadPoolDemo {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 100; i++) {
            executorService.execute(new Task());
        }
    }
}


