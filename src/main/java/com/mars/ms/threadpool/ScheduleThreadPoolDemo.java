package com.mars.ms.threadpool;

import java.util.concurrent.*;

/**
 * author by: mars
 * Date: 2022/3/19 23:23
 * Description:
 * newCachedThreadPool: 无界线程池，自动回收多余线程功能
 * new ThreadPoolExecutor(
 *          corePoolSize,                  -- 核心线程数
 *          Integer.MAX_VALUE,  -- 最大线程数
 *          0L, TimeUnit.MILLISECONDS,  -- keeplivetime，空闲线程存活时间
 *          new DelayedWorkQueue<Runnable>());  -- 工作队列
 */
public class ScheduleThreadPoolDemo {

    public static void main(String[] args) {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(10);
        //scheduledExecutorService.schedule(new Task(),2, TimeUnit.SECONDS); // 一次性
        scheduledExecutorService.scheduleAtFixedRate(new Task(),1,3,TimeUnit.SECONDS); // 周期性重复执行
    }
}


