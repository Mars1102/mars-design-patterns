package com.mars.ms.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * author by: mars
 * Date: 2022/3/19 23:23
 * Description:
 *
 * newFixedThreadPool 创建：
 * new ThreadPoolExecutor(
 *          nThreads,  -- 核心线程数
 *          nThreads,  -- 最大线程数
 *          0L, TimeUnit.MILLISECONDS,  -- keeplivetime，空闲线程存活时间
 *          new LinkedBlockingQueue<Runnable>());  -- 工作队列
 */
public class FixedThreadPoolDemo {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        for (int i = 0; i < 100; i++) {
            executorService.execute(new Task());
        }
    }
}

class Task implements Runnable{
    @Override
    public void run() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        System.out.println(Thread.currentThread().getName() + ":我被执行啦");
    }
}
