package com.mars.ms.threadpool;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * author by: mars
 * Date: 2022/3/20 23:52
 * Description:
 */
public class ShutDownExecutorsDemo {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 1000; i++) {
            executorService.execute(new ShutDownTask());
        }
        // 休眠1500ms
        Thread.sleep(1500L);
        // 停止线程池-shutdown 并没有让线程池停止，而是让未执行完毕的任务执行完，新加入的线程任务被拒绝
        //executorService.shutdown();
        // 向线程池加入任务，被拒绝
        //executorService.execute(new ShutDownTask());
        // 立刻停止线程池
        List<Runnable> runnables =  executorService.shutdownNow();
    }
}

class ShutDownTask implements Runnable{

    @Override
    public void run() {
        try {
            Thread.sleep(500);
            System.out.println(Thread.currentThread().getName());
        } catch (InterruptedException e) {
            //e.printStackTrace();
            System.out.println(Thread.currentThread().getName() + " , 被中断了");
        }

    }
}